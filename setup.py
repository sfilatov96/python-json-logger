from setuptools import setup

with open('README.rst') as file:
    long_description = file.read()

setup(
    name='ch-json-logger',
    version='1.1.4',

    description='JSON logger for python',
    long_description=long_description,

    url='https://bitbucket.org/sfilatov96/python-json-logger',

    author='Stepan Filatov',
    author_email='stepa-filatov@yandex.ru',

    license='Apache 2.0',

    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'License :: OSI Approved :: Apache Software License',

        'Intended Audience :: Developers',
        'Topic :: Software Development :: Libraries :: Python Modules',
        'Topic :: Utilities',

        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6'
    ],

    keywords='json logger python flask development kibana',

    packages=['ch_json_logger'],

    install_requires=[
        'JSON-log-formatter >= 0.2.0'
    ],
    python_requires='>=3.5',
)

